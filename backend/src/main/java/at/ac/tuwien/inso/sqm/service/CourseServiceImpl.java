package at.ac.tuwien.inso.sqm.service;

import at.ac.tuwien.inso.sqm.dto.AddCourseForm;
import at.ac.tuwien.inso.sqm.dto.CoruseDetailsForStudent;
import at.ac.tuwien.inso.sqm.dto.SemesterDto;
import at.ac.tuwien.inso.sqm.entity.Grade;
import at.ac.tuwien.inso.sqm.entity.LecturerEntity;
import at.ac.tuwien.inso.sqm.entity.Lehrveranstaltung;
import at.ac.tuwien.inso.sqm.entity.Rolle;
import at.ac.tuwien.inso.sqm.entity.Semester;
import at.ac.tuwien.inso.sqm.entity.StudentEntity;
import at.ac.tuwien.inso.sqm.entity.Subjcet;
import at.ac.tuwien.inso.sqm.entity.SubjectForStudyPlanEntity;
import at.ac.tuwien.inso.sqm.entity.Tag;
import at.ac.tuwien.inso.sqm.entity.UserAccountEntity;
import at.ac.tuwien.inso.sqm.exception.BusinessObjectNotFoundException;
import at.ac.tuwien.inso.sqm.exception.ValidationException;
import at.ac.tuwien.inso.sqm.repository.CourseRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.StudentRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.SubjectForStudyPlanRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.SubjectRepositoryInterface;
import at.ac.tuwien.inso.sqm.service.student_subject_prefs.StudentSubjectPreferenceStoreInterface;
import at.ac.tuwien.inso.sqm.validator.CourseValidator;
import at.ac.tuwien.inso.sqm.validator.ValidatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Service
public class CourseServiceImpl implements LehrveranstaltungServiceInterface {

    private static final Logger LOG = LoggerFactory.getLogger(CourseServiceImpl.class);
    private static final String LECTURER_EDIT_ERROR_NOT_ALLOWED =
            "lecturer.course.edit.error.notallowed";
    private ValidatorFactory validatorFactory = new ValidatorFactory();
    private CourseValidator validator = validatorFactory.getCourseValidator();

    @Autowired
    private SemesterServiceInterface semesterService;

    @Autowired
    private CourseRepositoryInterface courseRepository;

    @Autowired
    private SubjectRepositoryInterface subjectRepository;

    @Autowired
    private StudentRepositoryInterface stduentRepository;

    @Autowired
    private UserAccountServiceInterface userAccountService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private GradeServiceInterface gradeService;

    @Autowired
    private TgaServiceInterface tgaService;

    @Autowired
    private SubjectForStudyPlanRepositoryInterface subjectForStudyPlanRepository;

    @Autowired
    private StudentSubjectPreferenceStoreInterface studentSubjectPreferenceStore;

    @Override
    @Transactional(readOnly = true)
    public Page<Lehrveranstaltung> findCourseForCurrentSemesterWithName(
            @NotNull String name, Pageable pageable) {
        LOG.info("try to find course for current semester with semestername: " + name +
                 "and pageable " + pageable);
        Semester semester = semesterService.getOrCreateCurrentSemester().toEntity();
        return courseRepository
                .findAllBySemesterAndSubjectNameLikeIgnoreCase(semester, "%" + name + "%",
                                                               pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lehrveranstaltung> findCoursesForCurrentSemesterForLecturer(
            LecturerEntity lecturer) {
        LOG.info("try finding courses for current semester for lecturer with id " +
                 lecturer.getId());
        Semester semester = semesterService.getOrCreateCurrentSemester().toEntity();
        Iterable<Subjcet> subjectsForLecturer =
                subjectRepository.findByLecturers_Id(lecturer.getId());
        List<Lehrveranstaltung> courses = new ArrayList<>();
        subjectsForLecturer.forEach(subject -> courses
                .addAll(courseRepository.findAllBySemesterAndSubject(semester, subject)));
        return courses;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lehrveranstaltung> findCoursesForSubject(Subjcet subject) {
        LOG.info("try finding course for subject with id " + subject.getId());
        return courseRepository.findAllBySubject(subject);
    }

    @Override
    public List<Lehrveranstaltung> findCoursesForSubjectAndCurrentSemester(
            Subjcet subject) {
        List<Lehrveranstaltung> result = courseRepository.findAllBySemesterAndSubject(
                semesterService.getCurrentSemester().toEntity(), subject);
        return result;
    }

    @Override
    @Transactional
    public void dismissCourse(StudentEntity student, Long courseId) {
        Lehrveranstaltung course = findeLehrveranstaltung(courseId);
        student.addDismissedCourse(course);
    }

    @Override
    @Transactional
    public Lehrveranstaltung saveCourse(AddCourseForm form) {
        LOG.info("try saving course");
        Lehrveranstaltung course = form.getCourse();
        validator.validateNewCourse(course);

        UserAccountEntity u = userAccountService.getCurrentLoggedInUser();

        isLecturerAllowedToChangeCourse(course, u);

        LOG.info("try saving course " + course.toString());

        ArrayList<Tag> currentTagsOfCourse = new ArrayList<>(form.getCourse().getTags());

        for (String tag : form.getTags()) {
            Tag newTag = tgaService.findByName(tag);

            // tag doesn't exist, so create a new one.
            if (newTag == null) {
                course.addTags(new Tag(tag));
            }
            // tag exists, but not in this course
            else if (!course.getTags().contains(newTag)) {
                course.addTags(newTag);
            }
            // tag already exists for this course
            else {
                currentTagsOfCourse.remove(newTag);
            }
        }

        // the remaining tags are to be removed
        course.removeTags(currentTagsOfCourse);

        if (!(course.getStudentLimits() > 0)) {
            course.setStudentLimits(1);
        }
        return courseRepository.save(course);
    }

    private void isLecturerAllowedToChangeCourse(Lehrveranstaltung c,
                                                 UserAccountEntity u) {
        if (c == null || u == null) {
            String msg = messageSource
                    .getMessage(LECTURER_EDIT_ERROR_NOT_ALLOWED, null,
                                LocaleContextHolder.getLocale());
            throw new ValidationException(msg);
        }

        if (u.hasRole(Rolle.ADMIN)) {
            LOG.info("user is admin, therefore course modification is allowed");
            return;
        }

        for (LecturerEntity l : c.getSubject().getLecturers()) {
            if (l.getAccount().equals(u)) {
                LOG.info("found equal lecturers, course modification is allowed");
                return;
            }
        }
        LOG.warn(
                "suspisious try to modify course. user is not admin or does not own the subject for this course");
        String msg = messageSource
                .getMessage(LECTURER_EDIT_ERROR_NOT_ALLOWED, null,
                            LocaleContextHolder.getLocale());
        throw new ValidationException(msg);
    }

    @Override
    @Transactional(readOnly = true)
    public Lehrveranstaltung findeLehrveranstaltung(Long id) {
        LOG.info("try finding course with id " + id);
        Lehrveranstaltung course = courseRepository.findOne(id);
        if (course == null) {
            LOG.warn("Lehrveranstaltung with id " + id + " does not exist");
            throw new BusinessObjectNotFoundException(
                    "Lehrveranstaltung with id " + id + " does not exist");
        }
        return course;
    }

    @Override
    @Transactional
    public boolean remove(Long courseId) throws ValidationException {
        LOG.info("try removing  course with id " + courseId);
        validator.validateCourseId(courseId); // throws ValidationException
        Lehrveranstaltung course = courseRepository.findOne(courseId);

        if (course == null) {
            String msg =
                    "Lehrveranstaltung can not be deleted because there is no couse found with id " +
                    courseId;
            LOG.warn(msg);
            throw new BusinessObjectNotFoundException(msg);
        }

        isLecturerAllowedToChangeCourse(course,
                                        userAccountService.getCurrentLoggedInUser());


        List<Grade> grades = gradeService.findAllByCourseId(courseId);

        if (grades != null && grades.size() > 0) {
            String msg = "There are grades for course [id:" + courseId +
                         "], therefore integrationtest can not be removed.";
            LOG.warn(msg);
            throw new ValidationException(msg);
        }

        if (course.getStudents().size() > 0) {
            String msg = "There are students for course [id:" + courseId +
                         "], therefore integrationtest can not be removed.";
            LOG.warn(msg);
            throw new ValidationException(msg);
        }

        LOG.info("successfully validated course removal. removing now!");
        courseRepository.delete(course);
        return true;
    }


    @Override
    @Transactional
    public boolean studentZurLehrveranstaltungAnmelden(
            Lehrveranstaltung lehrveranstaltung) {
        validator.validateCourse(lehrveranstaltung);
        validator.validateCourseId(lehrveranstaltung.getId());
        StudentEntity student = stduentRepository.findByUsername(
                userAccountService.getCurrentLoggedInUser().getUsername());

        LOG.info(
                "try registering currently logged in student with id " + student.getId() +
                " for course with id " + lehrveranstaltung.getId());
        if (lehrveranstaltung.getStudentLimits() <=
            lehrveranstaltung.getStudents().size()) {
            return false;
        } else if (lehrveranstaltung.getStudents().contains(student)) {
            return true;
        } else {
            lehrveranstaltung.addStudents(student);
            courseRepository.save(lehrveranstaltung);
            studentSubjectPreferenceStore
                    .studentRegisteredCourse(student, lehrveranstaltung);
            return true;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lehrveranstaltung> findAllForStudent(StudentEntity student) {
        LOG.info("finding all courses for student with id " + student.getId());
        return courseRepository.findAllForStudent(student);
    }

    @Override
    @Transactional
    public Lehrveranstaltung studentVonLehrveranstaltungAbmelden(StudentEntity student,
                                                                 Long lehrveranstaltungsID) {
        LOG.info("Unregistering student with id {} from course with id {}",
                 student.getId(), lehrveranstaltungsID);
        validator.validateCourseId(lehrveranstaltungsID);

        Lehrveranstaltung course = courseRepository.findOne(lehrveranstaltungsID);
        if (course == null) {
            LOG.warn("Lehrveranstaltung with id {} not found. Nothing to unregister",
                     lehrveranstaltungsID);
            throw new BusinessObjectNotFoundException();
        }

        UserAccountEntity currentLoggedInUser =
                userAccountService.getCurrentLoggedInUser();
        //students should only be able to unregister themselves
        if (currentLoggedInUser.hasRole(Rolle.STUDENT)) {
            if (!student.getAccount().getUsername()
                        .equals(userAccountService.getCurrentLoggedInUser()
                                                  .getUsername())) {
                LOG.warn(
                        "student with id {} and username {} tried to unregister another one with id {} and username {}",
                        userAccountService.getCurrentLoggedInUser().getId(),
                        userAccountService.getCurrentLoggedInUser().getUsername(),
                        student.getId(), student.getAccount().getUsername());
                String msg = messageSource
                        .getMessage(LECTURER_EDIT_ERROR_NOT_ALLOWED, null,
                                    LocaleContextHolder.getLocale());
                throw new ValidationException(msg);
            }
        }

        //Lectureres should only be able to remove students from their own courses
        if (currentLoggedInUser != null && currentLoggedInUser.hasRole(Rolle.LECTURER)) {
            isLecturerAllowedToChangeCourse(course,
                                            userAccountService.getCurrentLoggedInUser());
        }


        course.removeStudents(student);
        studentSubjectPreferenceStore.studentUnregisteredCourse(student, course);
        return course;
    }

    private boolean checkRole(Rolle role) {
        return userAccountService.getCurrentLoggedInUser().hasRole(role);
    }

    @Override
    @Transactional(readOnly = true)
    public CoruseDetailsForStudent courseDetailsFor(StudentEntity student,
                                                    Long courseId) {
        validator.validateCourseId(courseId);
        validator.validateStudent(student);
        LOG.info("reading course details for student with id " + student.getId() +
                 " from course with id " + courseId);
        Lehrveranstaltung course = findeLehrveranstaltung(courseId);
        if (course == null) {
            LOG.warn("Lehrveranstaltung with id {} not found. Nothing to unregister",
                     courseId);
            throw new BusinessObjectNotFoundException();
        }


        return new CoruseDetailsForStudent(course)
                .setCanEnroll(canEnrollToCourse(student, course)).setStudyplans(
                        subjectForStudyPlanRepository.findBySubject(course.getSubject()));
    }

    @Override
    public List<SubjectForStudyPlanEntity> getSubjectForStudyPlanList(
            Lehrveranstaltung course) {
        validator.validateCourse(course);
        return subjectForStudyPlanRepository.findBySubject(course.getSubject());
    }

    private boolean canEnrollToCourse(StudentEntity student, Lehrveranstaltung course) {
        validator.validateCourse(course);
        validator.validateStudent(student);
        return course.getSemester().toDto()
                     .equals(semesterService.getOrCreateCurrentSemester()) &&
               !courseRepository.existsCourseRegistration(student, course);
    }

    @Override
    public List<Lehrveranstaltung> findAllCoursesForCurrentSemester() {
        SemesterDto semester = semesterService.getOrCreateCurrentSemester();
        return courseRepository.findAllBySemester(semester.toEntity());
    }
}
