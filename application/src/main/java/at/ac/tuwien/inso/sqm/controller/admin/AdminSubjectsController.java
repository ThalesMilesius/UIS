package at.ac.tuwien.inso.sqm.controller.admin;

import at.ac.tuwien.inso.sqm.controller.admin.forms.AddLecturersToSubjectForm;
import at.ac.tuwien.inso.sqm.controller.admin.forms.CreateSubjectForm;
import at.ac.tuwien.inso.sqm.entity.Subjcet;
import at.ac.tuwien.inso.sqm.exception.ValidationException;
import at.ac.tuwien.inso.sqm.service.Nachrichten;
import at.ac.tuwien.inso.sqm.service.SubjectServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

import static at.ac.tuwien.inso.sqm.controller.Constants.MAX_PAGE_SIZE;

@Controller
@RequestMapping("/admin/subjects")
public class AdminSubjectsController {
    private static final String REDIRECT = "redirect:/admin/subjects";
    private static final String FLASH_MESSAGE = "flashMessage";
    private static final Logger LOGGER =
            LoggerFactory.getLogger(AdminSubjectsController.class);

    @Autowired
    private SubjectServiceInterface subjectService;

    @Autowired
    private Nachrichten messages;


    @GetMapping
    public String listSubjects(
            @RequestParam(value = "search", required = false) String search,
            Model model
    ) {
        if ("".equals(search)) {
            return REDIRECT;
        }

        return listSubjectsForPageInternal(search, 1, model);
    }

    @GetMapping("/page/{pageNumber}")
    public String listSubjectsForPage(
            @RequestParam(value = "search", required = false) String search,
            @PathVariable Integer pageNumber,
            Model model
    ) {
        if (search == null && pageNumber == 1) {
            return REDIRECT;
        }

        if ("".equals(search)) {
            return "redirect:/admin/subjects/page/" + pageNumber;
        }

        if (pageNumber == 1) {
            return "redirect:/admin/subjects?search=" + search;
        }

        return listSubjectsForPageInternal(search, pageNumber, model);
    }

    /**
     * Does all the work for listSubjects and listSubjectsForPage
     */
    private String listSubjectsForPageInternal(String search, int pageNumber,
                                               Model model) {
        if (search == null) {
            search = "";
        }

        // Page numbers in the URL start with 1
        PageRequest page = new PageRequest(pageNumber - 1, MAX_PAGE_SIZE);

        Page<Subjcet> subjectsPage = subjectService.findBySearch(search, page);
        List<Subjcet> subjects = subjectsPage.getContent();

        // If the user tries to access a page that doesn't exist
        if (subjects.size() == 0 && subjectsPage.getTotalElements() != 0) {
            int lastPage = subjectsPage.getTotalPages();
            return "redirect:/admin/subjects/page/" + lastPage + "?search=" + search;
        }

        model.addAttribute("subjects", subjects);
        model.addAttribute("page", subjectsPage);
        model.addAttribute("search", search);

        return "admin/subjects";
    }


    @GetMapping("/{id}")
    public String getSubject(
            @PathVariable Long id,
            Model model,
            AddLecturersToSubjectForm addLecturersToSubjectForm,
            RedirectAttributes redirectAttributes
    ) {
        Subjcet subject = subjectService.findOne(id);
        model.addAttribute("subject", subject);

        if (subject == null) {
            LOGGER.info("/admin/subjects: Subjcet with id " + id + " not found");

            String msgId = "admin.subjects.notFound";
            redirectAttributes.addFlashAttribute(FLASH_MESSAGE, msgId);

            return REDIRECT;
        }

        model.addAttribute("lecturers", subject.getLecturers());
        return "admin/subject-details";
    }

    @PostMapping("/create")
    public String createSubject(@Valid CreateSubjectForm form,
                                 BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes
                    .addFlashAttribute(FLASH_MESSAGE, "admin.subjects.create.error");
            return REDIRECT;
        }
        Subjcet subject = subjectService.create(form.toSubject());
        redirectAttributes.addFlashAttribute("flashMessageNotLocalized",
                                             messages.msg("admin.subjects.create.success",
                                                          subject.getName()));

        return REDIRECT + subject.getId();
    }

    @PostMapping("/remove/{id}")
    public String removeSubject(@PathVariable Long id,
                                 RedirectAttributes redirectAttributes) {
        Subjcet subject = subjectService.findOne(id);
        if (subject == null) {
            redirectAttributes.addFlashAttribute(FLASH_MESSAGE,
                                                 "admin.subjects.remove.error.nosubjectfound");
            return REDIRECT;
        } else {
            try {
                subjectService.remove(subject);
            } catch (ValidationException e) {
                redirectAttributes.addFlashAttribute(FLASH_MESSAGE,
                                                     "admin.subjects.remove.error.courseexists");
                return REDIRECT;
            }
        }

        redirectAttributes.addFlashAttribute("flashMessageNotLocalized",
                                             messages.msg("admin.subjects.remove.success",
                                                          subject.getName()));
        return REDIRECT;
    }
}
