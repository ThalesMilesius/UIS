package at.ac.tuwien.inso.sqm.service;

import at.ac.tuwien.inso.sqm.entity.StudentEntity;
import at.ac.tuwien.inso.sqm.repository.StudentRepositoryInterface;
import at.ac.tuwien.inso.sqm.service.course_recommendation.StudentNeighborhoodStoreInterface;
import at.ac.tuwien.inso.sqm.service.course_recommendation.StudentSimilarityServiceInterface;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class StduentSimilarityServiceImpl implements StudentSimilarityServiceInterface {

    @Autowired
    private StudentNeighborhoodStoreInterface studentNeighborhoodStore;

    @Autowired
    private StudentRepositoryInterface stduentRepository;

    @Override
    public List<StudentEntity> getSimilarStudents(StudentEntity student) {
        UserNeighborhood userNeighborhood =
                studentNeighborhoodStore.getStudentNeighborhood();
        long[] userIds;
        try {
            userIds = userNeighborhood.getUserNeighborhood(student.getId());
        } catch (TasteException e) {
            return Collections.emptyList();
        }

        ArrayList<StudentEntity> students = new ArrayList<>();
        Arrays.stream(userIds).forEach(id -> students.add(stduentRepository.findOne(id)));

        return students;
    }
}
