package at.ac.tuwien.inso.sqm.controller.lecturer;

import at.ac.tuwien.inso.sqm.dto.GradeAuhtorizationDTO;
import at.ac.tuwien.inso.sqm.entity.Lehrveranstaltung;
import at.ac.tuwien.inso.sqm.entity.MarkEntity;
import at.ac.tuwien.inso.sqm.entity.StudentEntity;
import at.ac.tuwien.inso.sqm.exception.ValidationException;
import at.ac.tuwien.inso.sqm.service.FeedbackServiceInterface;
import at.ac.tuwien.inso.sqm.service.GradeServiceInterface;
import at.ac.tuwien.inso.sqm.service.LehrveranstaltungServiceInterface;
import at.ac.tuwien.inso.sqm.service.Nachrichten;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/lecturer/course-details")
public class LecturerCourseDetailsController {

    private static final String COURSE = "course";
    @Autowired
    private LehrveranstaltungServiceInterface courseService;

    @Autowired
    private GradeServiceInterface gradeService;

    @Autowired
    private FeedbackServiceInterface feedbackService;

    @Autowired
    private Nachrichten messages;

    @GetMapping
    public String getCourseDetails(@RequestParam("courseId") Long courseId,
                                    Model model) {
        Lehrveranstaltung course = courseService.findeLehrveranstaltung(courseId);
        model.addAttribute(COURSE, course);
        model.addAttribute("studyPlans",
                           courseService.getSubjectForStudyPlanList(course));
        return "lecturer/course-details";
    }

    @GetMapping("registrations")
    public String getCourseRegistrations(@RequestParam("courseId") Long courseId,
                                          Model model) {
        Lehrveranstaltung course = courseService.findeLehrveranstaltung(courseId);
        model.addAttribute(COURSE, course);
        model.addAttribute("students", course.getStudents());
        return "lecturer/course-registrations";
    }

    @GetMapping("issued-grades")
    public String getIssuedGrades(@RequestParam("courseId") Long courseId, Model model) {
        Lehrveranstaltung course = courseService.findeLehrveranstaltung(courseId);
        model.addAttribute(COURSE, course);
        model.addAttribute("grades",
                           gradeService.getGradesForCourseOfLoggedInLecturer(courseId));
        return "lecturer/issued-grades";
    }

    @GetMapping("feedback")
    public String getCourseFeedback(@RequestParam("courseId") Long courseId,
                                     Model model) {
        Lehrveranstaltung course = courseService.findeLehrveranstaltung(courseId);
        model.addAttribute(COURSE, course);
        model.addAttribute("feedbacks", feedbackService.findFeedbackForCourse(courseId));
        return "lecturer/course-feedback";
    }

    @PostMapping("addGrade")
    public String saveGrade(RedirectAttributes redirectAttributes,
                            @RequestParam("courseId") Long courseId,
                            @RequestParam("studentId") Long studentId,
                            @RequestParam("authCode") String authCode,
                            @RequestParam("mark") Integer mark) {

        GradeAuhtorizationDTO dto = gradeService
                .getDefaultGradeAuthorizationDTOForStudentAndCourse(studentId, courseId);
        dto.getGrade().setMark(MarkEntity.of(mark));
        dto.setAuthCode(authCode);
        StudentEntity student = dto.getGrade().getStudent();
        try {
            gradeService.saveNewGradeForStudentAndCourse(dto);
            String successMsg = messages.msg(
                    "lecturer.courses.registrations.issueCertificate.success",
                    student.getName());
            redirectAttributes.addFlashAttribute("flashMessageNotLocalized", successMsg);
        } catch (ValidationException e) {
            redirectAttributes
                    .addFlashAttribute("flashMessageNotLocalized", e.getMessage());
            return "redirect:/lecturer/course-details/registrations?courseId=" + courseId;
        }

        return "redirect:/lecturer/course-details/issued-grades?courseId=" + courseId;
    }
}
