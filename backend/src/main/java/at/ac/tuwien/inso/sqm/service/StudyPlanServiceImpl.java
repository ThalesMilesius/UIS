package at.ac.tuwien.inso.sqm.service;

import at.ac.tuwien.inso.sqm.entity.Grade;
import at.ac.tuwien.inso.sqm.entity.StduyPlanEntity;
import at.ac.tuwien.inso.sqm.entity.Subjcet;
import at.ac.tuwien.inso.sqm.entity.SubjectForStudyPlanEntity;
import at.ac.tuwien.inso.sqm.entity.SubjectTyppe;
import at.ac.tuwien.inso.sqm.entity.SubjectWithGrade;
import at.ac.tuwien.inso.sqm.exception.BusinessObjectNotFoundException;
import at.ac.tuwien.inso.sqm.repository.StudyPlanRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.SubjectForStudyPlanRepositoryInterface;
import at.ac.tuwien.inso.sqm.validator.StudyPlanValidator;
import at.ac.tuwien.inso.sqm.validator.ValidatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class StudyPlanServiceImpl implements StudyPlanServiceInterface {

    private static final Logger LOG = LoggerFactory.getLogger(StudyPlanServiceImpl.class);

    private StudyPlanRepositoryInterface studyPlanRepository;
    private SubjectForStudyPlanRepositoryInterface subjectForStudyPlanRepository;
    private SubjectServiceInterface subjectService;
    private MessageSource messageSource;
    private GradeServiceInterface gradeService;
    private ValidatorFactory validatorFactory = new ValidatorFactory();
    private StudyPlanValidator validator = validatorFactory.getStudyPlanValidator();

    @Autowired
    public StudyPlanServiceImpl(
            StudyPlanRepositoryInterface studyPlanRepository,
            SubjectForStudyPlanRepositoryInterface subjectForStudyPlanRepository,
            SubjectServiceInterface subjectService,
            GradeServiceInterface gradeService,
            MessageSource messageSource) {
        this.studyPlanRepository = studyPlanRepository;
        this.subjectForStudyPlanRepository = subjectForStudyPlanRepository;
        this.subjectService = subjectService;
        this.gradeService = gradeService;
        this.messageSource = messageSource;
    }

    @Override
    @Transactional
    public StduyPlanEntity create(StduyPlanEntity studyPlan) {
        LOG.info("creating stduyplan " + studyPlan.toString());
        validator.validateNewStudyPlan(studyPlan);
        return studyPlanRepository.save(studyPlan);
    }

    @Override
    @Transactional(readOnly = true)
    public List<StduyPlanEntity> findAll() {
        LOG.info("getting all studyplans");
        Iterable<StduyPlanEntity> studyplans = studyPlanRepository.findAll();

        return StreamSupport
                .stream(studyplans.spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public StduyPlanEntity findOne(Long id) {
        LOG.info("trying to find one stduyplan by id " + id);
        validator.validateStudyPlanId(id);
        StduyPlanEntity studyPlan = studyPlanRepository.findOne(id);
        if (studyPlan == null) {
            String msg = messageSource.getMessage("error.studyplan.notfound", null,
                                                  LocaleContextHolder.getLocale());
            LOG.warn("no stduyplan was found by the given id " + id);
            throw new BusinessObjectNotFoundException(msg);
        }
        return studyPlan;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SubjectForStudyPlanEntity> getSubjectsForStudyPlan(Long id) {
        validator.validateStudyPlanId(id);
        LOG.info("get subjects for studypolan by id " + id);
        return subjectForStudyPlanRepository
                .findByStudyPlanIdOrderBySemesterRecommendation(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<SubjectWithGrade> getSubjectsWithGradesForStudyPlan(Long id) {
        validator.validateStudyPlanId(id);
        LOG.info("getting subjects with grades for stduyplan width id " + id);
        List<SubjectForStudyPlanEntity> subjectsForStudyPlan =
                subjectForStudyPlanRepository
                        .findByStudyPlanIdOrderBySemesterRecommendation(id);
        List<Grade> grades = gradeService.getGradesForLoggedInStudent();
        List<SubjectWithGrade> subjectsWithGrades = new ArrayList<>();

        for (SubjectForStudyPlanEntity subjectForStudyPlan : subjectsForStudyPlan) {

            if (grades.isEmpty()) {
                // means there are no (more) grades at all
                if (subjectForStudyPlan.getMandatory()) {
                    subjectsWithGrades.add(new SubjectWithGrade(subjectForStudyPlan,
                                                                SubjectTyppe.MANDATORY));
                } else {
                    subjectsWithGrades.add(new SubjectWithGrade(subjectForStudyPlan,
                                                                SubjectTyppe.OPTIONAL));
                }
            }

            //look for grades belonging to the actual subject
            for (int i = 0; i < grades.size(); i++) {
                Grade grade = grades.get(i);
                if (grade.getCourse().getSubject()
                         .equals(subjectForStudyPlan.getSubject())) {
                    // add to mandatory or optional subjects
                    if (subjectForStudyPlan.getMandatory()) {
                        subjectsWithGrades
                                .add(new SubjectWithGrade(subjectForStudyPlan, grade,
                                                          SubjectTyppe.MANDATORY));
                    } else {
                        subjectsWithGrades
                                .add(new SubjectWithGrade(subjectForStudyPlan, grade,
                                                          SubjectTyppe.OPTIONAL));
                    }
                    grades.remove(grade);
                    break;
                } else if (i == grades.size() - 1) {
                    // means we reached the end of the list. there is no grade for this subject
                    if (subjectForStudyPlan.getMandatory()) {
                        subjectsWithGrades.add(new SubjectWithGrade(subjectForStudyPlan,
                                                                    SubjectTyppe.MANDATORY));
                    } else {
                        subjectsWithGrades.add(new SubjectWithGrade(subjectForStudyPlan,
                                                                    SubjectTyppe.OPTIONAL));
                    }
                }
            }
        }

        //remaining unassigned grades are used as free choice subjects
        for (Grade grade : grades) {
            subjectsWithGrades.add(new SubjectWithGrade(grade, SubjectTyppe.FREE_CHOICE));
        }

        return subjectsWithGrades;
    }

    @Override
    @Transactional
    public void addSubjectToStudyPlan(SubjectForStudyPlanEntity subjectForStudyPlan) {

        validator.validateNewSubjectForStudyPlan(subjectForStudyPlan);
        LOG.info("adding subject to stduyplan with " + subjectForStudyPlan);

        subjectForStudyPlanRepository.save(subjectForStudyPlan);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Subjcet> getAvailableSubjectsForStudyPlan(Long id, String query) {
        LOG.info("getting available subjects for stduyplan with id " + id +
                 " and search word " + query);
        validator.validateStudyPlanId(id);
        List<SubjectForStudyPlanEntity> subjectsForStudyPlan =
                subjectForStudyPlanRepository
                        .findByStudyPlanIdOrderBySemesterRecommendation(id);
        List<Subjcet> subjectsOfStudyPlan = subjectsForStudyPlan
                .stream()
                .map(SubjectForStudyPlanEntity::getSubject)
                .collect(Collectors.toList());
        List<Subjcet> subjects = subjectService.searchForSubjects(query);

        return subjects.stream().filter(it -> !subjectsOfStudyPlan.contains(it))
                       .collect(Collectors.toList());

    }

    @Override
    @Transactional
    public StduyPlanEntity disableStudyPlan(Long id) {

        LOG.info("disabling study plan with id " + id);
        validator.validateStudyPlanId(id);
        StduyPlanEntity studyPlan = findOne(id);
        if (studyPlan == null) {
            String msg = messageSource.getMessage("error.studyplan.notfound", null,
                                                  LocaleContextHolder.getLocale());
            LOG.warn(msg);
            throw new BusinessObjectNotFoundException(msg);
        }
        studyPlan.setEnabled(false);
        studyPlanRepository.save(studyPlan);
        return studyPlan;
    }

    @Override
    @Transactional
    public void removeSubjectFromStudyPlan(StduyPlanEntity sp, Subjcet s) {
        validator.validateRemovingSubjectFromStudyPlan(sp, s);
        LOG.info("removing subject " + s.toString() + " from stduyplan " + sp.getName());

        List<SubjectForStudyPlanEntity> sfsp = subjectForStudyPlanRepository
                .findByStudyPlanIdOrderBySemesterRecommendation(sp.getId());
        for (SubjectForStudyPlanEntity each : sfsp) {
            if (each.getSubject().getId().equals(s.getId())) {
                subjectForStudyPlanRepository.delete(each);
                break;
            }
        }
    }
}
