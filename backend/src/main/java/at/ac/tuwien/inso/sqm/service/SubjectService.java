package at.ac.tuwien.inso.sqm.service;

import at.ac.tuwien.inso.sqm.entity.LecturerEntity;
import at.ac.tuwien.inso.sqm.entity.Lehrveranstaltung;
import at.ac.tuwien.inso.sqm.entity.Subjcet;
import at.ac.tuwien.inso.sqm.exception.LecturerNotFoundException;
import at.ac.tuwien.inso.sqm.exception.RelationNotfoundException;
import at.ac.tuwien.inso.sqm.exception.SubjectNotFoundException;
import at.ac.tuwien.inso.sqm.exception.ValidationException;
import at.ac.tuwien.inso.sqm.repository.LecturerRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.SubjectRepositoryInterface;
import at.ac.tuwien.inso.sqm.validator.SubjectValidator;
import at.ac.tuwien.inso.sqm.validator.UisUserValidator;
import at.ac.tuwien.inso.sqm.validator.ValidatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class SubjectService implements SubjectServiceInterface {

    private static final Logger LOG = LoggerFactory.getLogger(SubjectService.class);
    private ValidatorFactory validatorFactory = new ValidatorFactory();
    private SubjectValidator validator = validatorFactory.getSubjectValidator();
    private UisUserValidator userValidator = validatorFactory.getUisUserValidator();
    private static final String SUBJECT_WITH_ID_NOT_FOUNG =
            "Subject with id {} not found";
    @Autowired
    private SubjectRepositoryInterface subjectRepository;

    @Autowired
    private LehrveranstaltungServiceInterface courseService;

    @Autowired
    private LecturerRepositoryInterface lecturerRepository;

    @Override
    public Page<Subjcet> findBySearch(String search, Pageable pageable) {
        LOG.info("finding search by word " + search);
        String sqlSearch = "%" + search + "%";
        return subjectRepository.findAllByNameLikeIgnoreCase(sqlSearch, pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Subjcet findOne(Long id) {
        validator.validateSubjectId(id);
        LOG.info("finding subject by id " + id);
        Subjcet subject = subjectRepository.findOne(id);

        if (subject == null) {
            LOG.warn("Subjcet not found");
            //TODO throwing this will cause a security test fail, for whatever reason?
            //throw new SubjectNotFoundException();
        }

        return subject;
    }

    @Override
    @Transactional
    public Subjcet create(Subjcet subject) {
        validator.validateNewSubject(subject);
        LOG.info("creating subject " + subject.toString());
        return subjectRepository.save(subject);
    }

    @Override
    @Transactional
    public LecturerEntity addLecturerToSubject(Long subjectId, Long lecturerUisUserId) {

        userValidator.validateUisUserId(lecturerUisUserId);
        validator.validateSubjectId(subjectId);

        LOG.info("addLecturerToSubject for subject {} and lecturer {}", subjectId,
                 lecturerUisUserId);

        LecturerEntity lecturer = lecturerRepository.findById(lecturerUisUserId);

        if (lecturer == null) {
            String msg =
                    "LecturerEntity with user id " + lecturerUisUserId + " not found";
            LOG.info(msg);
            throw new LecturerNotFoundException(msg);
        }

        Subjcet subject = subjectRepository.findById(subjectId);

        if (subject == null) {
            String msg = "Subjcet with id " + lecturerUisUserId + " not found";
            LOG.info(msg);
            throw new SubjectNotFoundException(msg);
        }

        if (subject.getLecturers().contains(lecturer)) {
            return lecturer;
        }

        subject.addLecturers(lecturer);
        subjectRepository.save(subject);

        return lecturer;
    }


    @Override
    @Transactional(readOnly = true)
    public List<LecturerEntity> getAvailableLecturersForSubject(Long subjectId,
                                                                String search) {
        validator.validateSubjectId(subjectId);
        if (search == null) {
            search = "";
        }
        LOG.info("getting available lectureres for subject with subject id " + subjectId +
                 " and search string " + search);


        Subjcet subject = subjectRepository.findById(subjectId);

        if (subject == null) {
            String msg = MessageFormatter.format(SUBJECT_WITH_ID_NOT_FOUNG, subjectId)
                                         .getMessage();
            LOG.warn(msg);
            throw new SubjectNotFoundException(msg);
        }

        List<LecturerEntity> currentLecturers = subject.getLecturers();

        List<LecturerEntity> searchedLecturers =
                lecturerRepository
                        .findAllByIdentificationNumberLikeIgnoreCaseOrNameLikeIgnoreCase(
                                "%" + search + "%",
                                "%" + search + "%"
                        );

        return searchedLecturers
                .stream()
                .filter(lecturer -> !currentLecturers.contains(lecturer))
                .limit(10)
                .collect(Collectors.toList());
    }

    @Override
    public LecturerEntity removeLecturerFromSubject(Long subjectId,
                                                    Long lecturerUisUserId) {
        userValidator.validateUisUserId(lecturerUisUserId);
        validator.validateSubjectId(subjectId);
        LOG.info("removeLecturerFromSubject for subject {} and lecturer {}", subjectId,
                 lecturerUisUserId);

        Subjcet subject = subjectRepository.findById(subjectId);

        if (subject == null) {
            String msg = MessageFormatter.format(SUBJECT_WITH_ID_NOT_FOUNG, subjectId)
                                         .getMessage();
            LOG.warn(msg);
            throw new SubjectNotFoundException(msg);
        }

        LecturerEntity lecturer = lecturerRepository.findById(lecturerUisUserId);

        if (lecturer == null) {
            String msg = "LecturerEntity with id '" + lecturerUisUserId + "' not found";
            LOG.info(msg);
            throw new LecturerNotFoundException(msg);
        }

        List<LecturerEntity> currentLecturers = subject.getLecturers();

        boolean isLecturer = currentLecturers.contains(lecturer);

        if (!isLecturer) {
            String msg = "LecturerEntity with id " + lecturerUisUserId +
                         " not found for subject " +
                         subjectId;
            LOG.info(msg);
            throw new RelationNotfoundException(msg);
        }

        subject.removeLecturers(lecturer);

        subjectRepository.save(subject);

        return lecturer;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Subjcet> searchForSubjects(String word) {
        LOG.info("seachring for subjects with search word " + word);
        return subjectRepository.findByNameContainingIgnoreCase(word);
    }

    @Override
    @Transactional
    public boolean remove(Subjcet subject) throws ValidationException {
        LOG.info("trying to remove subject");
        validator.validateNewSubject(subject);
        if (subject == null) {
            LOG.info("Subjcet is null.");
            throw new ValidationException("Subjcet is null.");
        }
        LOG.info("removing subject " + subject + " now.");
        List<Lehrveranstaltung> courses = courseService.findCoursesForSubject(subject);
        if (courses.size() > 0) {
            String msg = "";
            if (subject != null) {
                msg = "Cannot delete subject [Name: " + subject.getName() +
                      "] because there are courses";
            }
            LOG.info(msg);
            throw new ValidationException(msg);
        } else {
            subjectRepository.delete(subject);
            return true;
        }
    }


}
