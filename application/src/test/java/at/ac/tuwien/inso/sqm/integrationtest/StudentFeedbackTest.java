package at.ac.tuwien.inso.sqm.integrationtest;

import static at.ac.tuwien.inso.sqm.entity.Feedback.Type.DISLIKE;
import static at.ac.tuwien.inso.sqm.entity.Feedback.Type.LIKE;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import at.ac.tuwien.inso.sqm.entity.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import at.ac.tuwien.inso.sqm.controller.student.forms.FeedbackForm;
import at.ac.tuwien.inso.sqm.entity.Feedback;
import at.ac.tuwien.inso.sqm.repository.CourseRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.FeedbackRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.GradeRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.LecturerRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.SemesterRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.StudentRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.SubjectRepositoryInterface;
import at.ac.tuwien.inso.sqm.repository.UisUserRepositoryInterface;
import at.ac.tuwien.inso.sqm.service.student_subject_prefs.StudentSubjectPreferenceStoreInterface;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Transactional
public class StudentFeedbackTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UisUserRepositoryInterface uisUserRepository;

    @Autowired
    private CourseRepositoryInterface courseRepository;

    @Autowired
    private SubjectRepositoryInterface subjectRepository;

    @Autowired
    private SemesterRepositoryInterface semesterRepository;

    @Autowired
    private FeedbackRepositoryInterface feedbackRepository;

    @Autowired
    private StudentRepositoryInterface stduentRepository;

    @Autowired
    private GradeRepositoryInterface gradeRepository;

    @Autowired
    private LecturerRepositoryInterface lecturerRepository;
    @Autowired
    private StudentSubjectPreferenceStoreInterface studentSubjectPreferenceStore;

    private StudentEntity student;
    private List<Lehrveranstaltung> courses;

    @Before
    public void setUp() {
        prepareStudent();
        prepareCourses();
    }

    private void prepareStudent() {
        student = uisUserRepository.save(new StudentEntity("1", "student", "email", new UserAccountEntity("student", "pass", Rolle.STUDENT)));
    }

    private void prepareCourses() {
        Subjcet subject = subjectRepository.save(new Subjcet("subject", BigDecimal.ONE));
        Semester semester = semesterRepository.save(new Semester(2016, SemestreTypeEnum.WINTER_SEMESTER));

        courses = StreamSupport.stream(courseRepository.save(asList( new Lehrveranstaltung(subject, semester), new Lehrveranstaltung(subject, semester).addStudents(student))).spliterator(), false).collect(Collectors.toList());

        studentSubjectPreferenceStore.studentRegisteredCourse(student, courses.get(1));
    }

    @Test
    public void itPersistsFeedbackFromStudentForCourseHeIsRegisteredTo() throws Exception {
        FeedbackForm form = new FeedbackForm(courses.get(1).getId(), false, "some description");

        mockMvc.perform(
                giveFeedback(form)
        ).andExpect(
                feedbackCreated(form)
        ).andExpect(
                redirectedUrl("/student/meineLehrveranstaltungen")
        ).andExpect(
                flash().attribute("flashMessage", "student.my.courses.feedback.success")
        );
    }

    private MockHttpServletRequestBuilder giveFeedback(FeedbackForm form) {
        return post("/student/feedback")
                .param("course", form.getCourse().toString())
                .param("suggestions", form.getSuggestions())
                .param("like", form.isLike().toString())
                .with(csrf())
                .with(user(student.getAccount()));
    }

    private ResultMatcher feedbackCreated(FeedbackForm form) {
        return result -> {
            List<Feedback> feedbacks = feedbackRepository.findAllOfStudent(student);
            assertThat(feedbacks, hasSize(1));

            Feedback feedback = feedbacks.get(0);
            assertThat(feedback.getSuggestions(), equalTo(form.getSuggestions()));
            assertThat(feedback.getType(), equalTo(form.isLike() ? LIKE : DISLIKE));
        };
    }

    @Test
    public void itDoesNotPersistsFeedbackFromStudentForCourseHeIsNotRegisteredTo() throws Exception {
        FeedbackForm form = new FeedbackForm(courses.get(0).getId(), true, "some description");

        mockMvc.perform(
                giveFeedback(form)
        ).andExpect(
                feedbackNotCreated()
        ).andExpect(
                status().isForbidden()
        );
    }

    private ResultMatcher feedbackNotCreated() {
        return result -> assertThat(feedbackRepository.findAllOfStudent(student), empty());
    }

    @Test
    public void itRespondsNotFoundOnFeedbackForUnknownCourse() throws Exception {
        FeedbackForm form = new FeedbackForm(-1L, false, "some description");

        mockMvc.perform(
                giveFeedback(form)
        ).andExpect(
                status().isNotFound()
        );
    }

    @Test
    public void itRespondsForbiddenOnMultipleFeedbackSubmissionsForSameCourse() throws Exception {
        feedbackRepository.save(new Feedback(student, courses.get(1)));

        FeedbackForm form = new FeedbackForm(courses.get(1).getId(), true, "some description");

        mockMvc.perform(
                giveFeedback(form)
        ).andExpect(
                status().isForbidden()
        );
    }

    @Test
    public void itDoesNotPersistFeedbackWithTooLongSuggestions() throws Exception {
        FeedbackForm form = new FeedbackForm(courses.get(1).getId(), true, tooLongSuggestions());

        mockMvc.perform(
                giveFeedback(form)
        ).andExpect(
                feedbackNotCreated()
        ).andExpect(
                status().isBadRequest()
        );
    }

    private String tooLongSuggestions() {
        StringBuilder builder = new StringBuilder();
        IntStream.range(0, 1025).forEach(it -> builder.append("a"));
        return builder.toString();
    }
}
